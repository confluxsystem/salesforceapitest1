Narrative: Inorder to find distance between two US zip Codes

Scenario: when a user determine the distance between two US zip codes
Given US Zip Code api is available
And Api Key is available
And multi-distance.format is available
And US Zip Code 1 is available
And Units is available
And US Zip Code 2 is available
When I send the Get Request via api
Then I should get 200 Ok status code with distance between two Zip Codes.

