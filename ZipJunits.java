package org.getzip;

import org.salesforceapi.JbehaveTest;

public class ZipJunits extends JUnitStories {

        @Override
        public Configuration configuration() {
            return new MostUsefulConfiguration()
                    .useStoryLoader(new LoadFromClasspath(this.getClass()))
                    .useStoryReporterBuilder(new StoryReporterBuilder()
                            .withCodeLocation(codeLocationFromClass(this.getClass()))
                            .withFormats(CONSOLE));
        }

        @Override
        public InjectableStepsFactory stepsFactory() {
            return new InstanceStepsFactory(configuration(), new JbehaveTest());
        }

        @Override
        protected List<String> storyPaths() {
            return Arrays.asList("ZipCodeApi.story");
        }

    }
}
