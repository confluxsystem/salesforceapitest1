package org.salesforceapi;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class JbehaveTest {
    private String api;
    private String apikey;
    private String multiDistance;
    private String zipCode1;
    private String zipCode2;
    private String units;
    private int getZipResponse;
    private int zipDistance;
    @Given("US Zip Code api is available")
    public void zipCodeApi() {
        api = "https://www.zipcodeapi.com/rest/DemoOnly001WFabkFurCLxym8V5eY8ZeJpSLyhDDsEsHGnepzkTeNys4QDiKlWHI/match-close.json/06404%2C55101/2000/mile";
    }

    @Given("Api Key is available")
    public void apiKey() {
        apikey = "DemoOnly001WFabkFurCLxym8V5eY8ZeJpSLyhDDsEsHGnepzkTeNys4QDiKlWHI";
    }

    @Given("multi-distance.format is available")
    public void multiDistanceFormat() {
        multiDistance = "match-close.format";

    }

    @Given("US Zip Code 1 is available")
    public void givenZipCode1() {
        zipCode1 = "06401";
    }
    @Given("US Zip Code 2 is available")
    public void givenZipCode2() {
        zipCode2 = "55101";
    }
    @Given("Units is available")
    public void setUnits() {
        units = "kms";

    }
    @When("I send the Get Request via api")
    public void whenSentGetRequest() throws IOException {
        getZipResponse = getZipCodeApi(api,apikey,multiDistance,zipCode1,zipCode2,units)

    }
    @Then("I should get 200 Ok status code with distance between two Zip Codes")
    public int getThenDistance() {
        assert(200,return getZipResponse);
    }
    private int getZipCodeApi(String api, String apikey, String multiDistance, String zipCode1, String zipCode2, String units) {
    return getZipCodeApi;
    }

}